﻿using System;

namespace ReferenceType
{
    class ReferenceType
    {
        public int valueVal;
       
    }

    class TestReference
    {
        public static void Main(string[] args)
        {
            ReferenceType referenceType = new ReferenceType();
            referenceType.valueVal = 5;
            Test(referenceType);
            Console.WriteLine("The value of the variable is {0}",referenceType.valueVal);
            Console.ReadLine();
        }

        public static void Test(ReferenceType refer)
        {
            int temp = 5;
            refer.valueVal = temp * 2;
            
        }
    }
}