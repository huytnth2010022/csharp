﻿using System;

namespace AnonTypes
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var p1 = new {Name = "a", Price = 3};
            Console.WriteLine("Name: {0}\nPrice: {1}",p1.Name.ToUpper(),p1.Price);
            Console.ReadLine();
        }
    }
}