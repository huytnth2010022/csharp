﻿using System;

namespace implicitily
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var i = 5;
            var s = "Hello";
            var d = 1.0;
            Console.WriteLine("i*i = {0}",i*i);
            Console.WriteLine(s);
            Console.WriteLine("type of d:"+d.GetType());
            Console.ReadLine();
        }
    }
}