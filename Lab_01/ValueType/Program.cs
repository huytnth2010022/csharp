﻿using System;

namespace ValueType
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            int valueVal = 6;
            Text(valueVal);
            Console.WriteLine("the value of the variable is {0}",valueVal);
            Console.ReadLine();
        }

        public static void Text(int Value)
        {
            int temp = 5;
            Value = temp + 2;
        }
    }
}