﻿using System;

namespace DataType
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            int inVal;
            double dbVal;
            string strVal;
            inVal = 10;
            dbVal = 10.00;
            strVal = "Hello C#";
            Console.WriteLine("{0} is an integer value",inVal);
            Console.WriteLine("{0} is an double value",dbVal);
            Console.WriteLine("{0} is an string value",strVal);
            Console.ReadLine();
        }
    }
}