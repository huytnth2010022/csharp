﻿using System;

namespace DoYourSelf4
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Nhập số Tự nhiên");
            int number = int.Parse(Console.ReadLine());
            for (int i = 1; i < 10; i++)
            {
                Console.WriteLine(number+"*"+i +"="+number*i);
            }
        }
    }
}