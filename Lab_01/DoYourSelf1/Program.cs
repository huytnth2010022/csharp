﻿using System;

namespace DoYourSelf1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Please Enter Your Name:");
            String name = Console.ReadLine();
            Console.WriteLine("Please Enter Your Address:");
            String address = Console.ReadLine();
            Console.WriteLine("Please Enter Your Phone:");
            int phone = int.Parse(Console.ReadLine());
            Console.WriteLine("Your information:\n{0}\n{1}\n{2}", name, address,phone);

        }
    }
}