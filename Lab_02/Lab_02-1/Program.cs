﻿using System;

namespace Lab_02_1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            DateTime now = DateTime.Now;
            Random rand = new Random((int) now.Millisecond);
            int[] Arr = new int[10];
            for (int x = 0; x < Arr.Length; x++)
            {
                Arr[x] = rand.Next() % 100;
            }

            int Total = 0;
            Console.WriteLine("Array values are ");
            foreach (var val  in Arr)
            {
                Total += val;
                Console.WriteLine(val + ", ");
            }

            Console.WriteLine("\n And the average is {0}",(double)Total/(double)Arr.Length);
            
        }
    }
}