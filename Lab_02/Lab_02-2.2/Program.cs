﻿using System;

namespace Lab_02_2._2
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            int[] arr = new int[12]
            {
                12,28,98,12,114,17,16,18,16,61,98,54
            };
            Console.WriteLine("the first occurrence of 98 is at index: "
            +Array.IndexOf(arr,98));
            Console.WriteLine("the last occurrence of 98 is at index: "+ 
                              Array.LastIndexOf(arr,98));
            int x = 0;
            while ((x = Array.IndexOf(arr,98,x)) >= 0)
            {
                Console.WriteLine("98 found at index " + x);
                x++;
            }

            x = arr.Length - 1;
            while ((x = Array.LastIndexOf(arr,98,x)) >=0)
            {
                Console.WriteLine("98 found at index "+x);
                x--;
            }

            Console.WriteLine("Array that before sorted");
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("{0}: {1}",i+1,arr[i]);
            }
            Array.Sort(arr);
            Console.WriteLine("Array that after sorted");
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("{0}: {1}",i+1,arr[i]);
            }
            Array.Reverse(arr);
            Console.WriteLine("Array that after reserse");
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("{0}: {1}",i+1,arr[i]);
            }

            Console.ReadLine();
        }
    }
}