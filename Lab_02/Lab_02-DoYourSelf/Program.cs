﻿using System;
using System.Collections.Generic;

namespace Lab_02_DoYourSelf
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            int number;
            do
            {
                
                number = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("SIN Validator");
                Console.WriteLine("=============");
                Console.WriteLine("SIN (0 to quit): " + number);
                int[] arrint = new int[9];
                for (int i = 0; i < arrint.Length; i++)
                {
                    int so = number % 10;
                    arrint[i] = so;
                    number = (number - so) / 10;
                }
                Array.Reverse(arrint);
                // for (int i = 0; i < arrint.Length; i++)
                // {
                //     Console.WriteLine(arrint[i].ToString());
                // }
                //chỉ làm việc với các phần tử 1 3 5 7 
                int sum1 = arrint[1] + arrint[1];
                int sum1_1 = 0;
                int sum3 = arrint[3] + arrint[3];
                int sum1_3=0;
                int sum5 = arrint[5] + arrint[5];
                int sum1_5=0;
                int sum7 = arrint[7] + arrint[7];
                int sum1_7=0;
                if (sum1 > 10)
                {
                    sum1_1 = sum1 % 10;
                    sum1 = (sum1 - sum1_1) / 10;
                }
                if (sum3 > 10)
                {
                    sum1_3 = sum3 % 10;
                    sum3 = (sum3 - sum1_3) / 10;
                }
                if (sum5 > 10)
                {
                    sum1_5 = sum5 % 10;
                    sum5 = (sum5 - sum1_5) / 10;
                }
                if (sum7 > 10)
                {
                    sum1_7 = sum7 % 10;
                    sum7 = (sum7 - sum1_7) / 10;
                }
                int digitsum = sum1 + sum1_1 + sum1_3 + sum3 + sum1_5 + sum5 + sum1_7 + sum7;
                int[] arrdigit = new int[5];
                int count=1;
                for (int i = 0; i < 5; i++)
                {
                    arrdigit[i] = count;
                    if (count < arrint[8])
                    {
                        count += 2;
                    }
                
                }
                for (int i = 0; i < 4; i++)
                {
                    int j = i + 1;
                    if (arrdigit[i] == arrdigit[j])
                    {
                        arrdigit[j] = 0;
                    }
                }

                int total = digitsum + arrdigit[0] + arrdigit[1] + arrdigit[2] + arrdigit[3] + arrdigit[4];
           
                int highestInterger = 10;
                do
                {
                    highestInterger = highestInterger + 10;
                } while ((highestInterger-total)<0);

                if ((highestInterger-total)==arrint[8])
                {
                    Console.WriteLine("This is a valid SIN.");
                 
                }
                else
                {
                    Console.WriteLine("This is not a valid SIN.");
                 
               
                }
                
            } while (number!=0);

            Console.WriteLine("Have a Nice Day!");
        }
        
    }
}