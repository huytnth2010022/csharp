﻿using System;
using System.Security.Cryptography;
using Bank.Controler;

namespace Bank.View
{
    public class UserView
    {
        private static UserControler _userControler = new UserControler();

       

            public void GetComeMenu()
            {
                int choise;
                do
                {
                    Console.WriteLine("------------SHBank-----------");
                    Console.WriteLine("1.Đăng ký");
                    Console.WriteLine("2.Đăng nhập");
                    Console.WriteLine("3.Xem thông tin Ngân hàng");
                    Console.WriteLine("4.Thoát");
                    choise = Convert.ToInt32(Console.ReadLine());
                    switch (choise)
                    {
                        case 1:
                            _userControler.Register();
                            break;
                        case 2:
                            _userControler.Login();
                            break;
                        case 3:
                            Console.WriteLine("Bạn Không nên biết về chúng tôi");
                            break;
                        case 4:
                            break;

                        default:
                            Console.WriteLine("Bạn đã không chọn gì !!");
                            Console.ReadLine();
                            break;
                    }


                } while (choise != 4);

                Console.WriteLine("Thank You 🤗🤗🤗");
            }

            public static void GetMainMenu()
            {
                int choise;
                do
                {
                    Console.WriteLine("Chọn tính năng: ");
                    Console.WriteLine("1. Kiểm tra thông tin");
                    Console.WriteLine("2.Update Thông tin");
                    Console.WriteLine("3.Kiểm tra lịch sử giao dịch");
                    Console.WriteLine("4.Khóa giao dịch");
                    Console.WriteLine("5.Mở khóa giao dịch");
                    Console.WriteLine("6.Đổi mật khẩu");
                    Console.WriteLine("7.Gửi tiền");
                    Console.WriteLine("8.Rút tiền");
                    Console.WriteLine("9.Chuyển tiền");
                    Console.WriteLine("10.Đăng xuất");
                    choise = Convert.ToInt32(Console.ReadLine());
                    switch (choise)
                    {
                        case 1:
                            _userControler.CheckInformatoin();
                            break;
                        case 2:
                            _userControler.UpdateInformation();
                            break;
                        case 3:
                            _userControler.CheckTransactionHistory();
                            break;
                        case 4:
                            _userControler.lockTransaction();
                            break;
                        case 5:
                            _userControler.UnlockTransaction();
                            break;
                        case 6:
                            _userControler.ChangePassword();
                            break;
                        case 7:
                            _userControler.deposit();
                            break;
                        case 8:
                            _userControler.withDraw();
                            break;
                        case 9:
                            _userControler.Transfer();
                            break;
                    }
                } while (choise != 10);


            }

          
    }
}