﻿using System;
using Bank.Controler;

namespace Bank.View
{
    public class AdminView:IAdminiView
    {
        private static AdminiControler _adminiControler = new AdminiControler();
        public void GetAdminMenu()
    {
        int choise;
        do
        {
            Console.WriteLine("------------SHBank-----------");
            Console.WriteLine("1.Đăng ký");
            Console.WriteLine("2.Đăng nhập");
            Console.WriteLine("3.Xem thông tin Ngân hàng");
            Console.WriteLine("4.Thoát");
            choise = Convert.ToInt32(Console.ReadLine());
            switch (choise)
            {
                case 1:
                    _adminiControler.CreateAdmini();
                    break;
                case 2:
                    _adminiControler.Login();
                    break;
                case 3:
                    Console.WriteLine("Bạn Không nên biết về chúng tôi");
                    break;
                case 4:
                    break;

                default:
                    Console.WriteLine("Bạn đã không chọn gì !!");
                    Console.ReadLine();
                    break;
            }


        } while (choise != 4);

        Console.WriteLine("Thank You 🤗🤗🤗");
    }
        public static void GetMainMenu()
            {
                int choise;
                do
                {
                    Console.WriteLine("Chọn tính năng: ");
                    Console.WriteLine("1.Tìm kiếm người dùng theo Mã số tài khoản");
                    Console.WriteLine("2.Tìm kiếm người dùng theo số điện thoại");
                    Console.WriteLine("3.Tra cứu lịch sử người dùng theo mã số");
                    Console.WriteLine("4.Khóa tài khoản");
                    Console.WriteLine("5.Mở khóa tài khoản");
                    Console.WriteLine("6.Hiện thị danh sách người dùng");
                    Console.WriteLine("7.Đăng xuất");
                    choise = Convert.ToInt32(Console.ReadLine());
                    switch (choise)
                    {
                 case  1:
                     _adminiControler.FindUserByAccountNumber();
                     break;
                 case  2:
                     _adminiControler.SearchUserByPhone();
                     break;
                 case  3:
                     _adminiControler.SearchTransactionHistory();
                     break;
                 case 4:
                     _adminiControler.LockUser();
                     break;
                 case 5:
                     _adminiControler.UnlockUser();
                     break;
                 case 6:
                     _adminiControler.ShowListUser();
                     break;
                 case 7:
                     break;
                    }
                } while (choise != 7);


            }
    }
}