﻿using System;
using Bank.Entity;
using Bank.Model;
using Bank.Util;
using Bank.View;

namespace Bank.Controler
{
    public class AdminiControler:IAdminiControler
    {
        private AcountModel _acountModel = new AcountModel();
        public Admini Login()
        {
            Admini admin = new Admini();
            //Lấy thông tin người dùng
            Console.WriteLine("Tài Khoản: ");
           admin.UserName = Console.ReadLine();
            Console.WriteLine("Mật khẩu: ");
            admin.PassWord = Console.ReadLine();
            //check tài khoản có tồn tại trên DB không đồng thời kéo dữ liệu từ DB để lấy Salt
            var adminDb = _acountModel.FindByAccounAdminName(admin.UserName);

            if (adminDb.UserName.Equals(admin.UserName))
            {
                string newPW = HashUtil.GenerateSaltedSHA1(admin.PassWord,adminDb.Salt);
                if (newPW.Equals(admin.PassWord))
                {
                    Console.WriteLine("Đăng nhập thành công");
                   AdminView.GetMainMenu();
                }
                else
                {
                    Console.WriteLine("Đăng nhập thất bại");
                }
            }
            else
            {
                Console.WriteLine("không tồn tại");
            }

            return admin;
        }

        public void CreateAdmini()
        {
            bool flag = false;
            Admini admin = new Admini();
            do
            {
                //Validate dữ liệu
                Console.WriteLine("Nhập tài khoản");
                admin.UserName = Console.ReadLine();
                Console.WriteLine("Nhập mật khẩu");
                admin.PassWord = Console.ReadLine();
                Console.WriteLine("Nhập lại mật khẩu");
                admin.PasswardConfig = Console.ReadLine();
                var errors = admin.IsValid();
                flag = admin.IsValid().Count == 0;
                if (!flag)
                {
                    foreach (var error in errors)
                    {
                        Console.WriteLine(error.Value);
                    }

                    Console.WriteLine("vui lòng bạn nhập lại thông tin!");
                    Console.ReadLine();
                }
            } while (!flag);
            admin.EncryptPassWord();
            var result = _acountModel.SaveAdmin(admin);
            if (result)
            {
                Console.WriteLine("Đăng ký thành công");
            }
            else
            {
                Console.WriteLine("Đăng ký thất bại");
            }
        }

   

        public void ShowListUser()
        {
            var listUser = _acountModel.FindAll();
            Console.WriteLine("====================================");
            foreach (var acount in listUser)
            {
                Console.WriteLine("Mã số tài khoản: "+acount.AccountNumber);
                Console.WriteLine("Tên chủ tài khoản: "+acount.firstName+" "+acount.lastName);
                Console.WriteLine("Số điện thoại: "+acount.Phone);
                Console.WriteLine("Địa chỉ: "+acount.Address);
                Console.WriteLine("Email: "+ acount.Email);
                Console.WriteLine("Ngày sinh: " + acount.Dab);
                Console.WriteLine("Số dư: "+acount.Balance);
                Console.WriteLine("CCCD/CMND: " + acount.IdentityNumber);
                Console.WriteLine("Trạng thái tài khoản: " + acount.GetStatus());
                Console.WriteLine("====================================");
            }
        }
        public void LockUser()
        {
            Console.WriteLine("Nhập Mã số tài khoản cần khóa: ");
            var AccountNumber = Console.ReadLine();
            var accountExisting = _acountModel.FindByAccountNumber(AccountNumber);
            if (accountExisting.AccountNumber.Length != 0)
            {
                accountExisting.Status = 2;
                _acountModel.LockAndUnLockAccount(accountExisting.GetStatus(), accountExisting.AccountNumber);
                Console.WriteLine("Khóa thành công");
            }
            else
            {
                Console.WriteLine("Tài khoản này không tồn tại");
            }
        }
        public void UnlockUser()
        {
            Console.WriteLine("Nhập Mã số tài khoản cần khóa: ");
            var AccountNumber = Console.ReadLine();
            var accountExisting = _acountModel.FindByAccountNumber(AccountNumber);
            if (accountExisting.AccountNumber.Length != 0)
            {
                accountExisting.Status = 1;
                _acountModel.LockAndUnLockAccount(accountExisting.GetStatus(), accountExisting.AccountNumber);
                Console.WriteLine("Mở khóa thành công");
            }
            else
            {
                Console.WriteLine("Tài khoản này không tồn tại");
            }
        }

        public void FindUserByAccountNumber()
        {
            Console.WriteLine("Nhập Mã số tài khoản cần khóa: ");
            var AccountNumber = Console.ReadLine();
            var acount = _acountModel.FindByAccountNumber(AccountNumber);
            if (acount.AccountNumber.Length != 0)
            {
                Console.WriteLine("-----Thông tin tài khoản của bạn là-----");
                Console.WriteLine("Tên chủ tài khoản: "+acount.firstName+" "+acount.lastName);
                Console.WriteLine("Số điện thoại: "+acount.Phone);
                Console.WriteLine("Địa chỉ: "+acount.Address);
                Console.WriteLine("Email: "+ acount.Email);
                Console.WriteLine("Ngày sinh: " + acount.Dab);
                Console.WriteLine("Số dư: "+acount.Balance);
                Console.WriteLine("CCCD/CMND: " + acount.IdentityNumber);
                Console.WriteLine("Trạng thái tài khoản: " + acount.GetStatus());
                Console.WriteLine("====================================================");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Tài khoản này không tồn tại");
            }
        }

        public void SearchUserByPhone()
        {
            Console.WriteLine("Nhập Mã số tài khoản cần khóa: ");
            var Phone = Console.ReadLine();
            var acounts = _acountModel.SearchByPhone(Phone);
            foreach (var acount in acounts)
            {
                Console.WriteLine("-----Thông tin tài khoản của bạn là-----");
                    Console.WriteLine("Tên chủ tài khoản: "+acount.firstName+" "+acount.lastName);
                    Console.WriteLine("Số điện thoại: "+acount.Phone);
                    Console.WriteLine("Địa chỉ: "+acount.Address);
                    Console.WriteLine("Email: "+ acount.Email);
                    Console.WriteLine("Ngày sinh: " + acount.Dab);
                    Console.WriteLine("Số dư: "+acount.Balance);
                    Console.WriteLine("CCCD/CMND: " + acount.IdentityNumber);
                    Console.WriteLine("Trạng thái tài khoản: " + acount.GetStatus());
                    Console.WriteLine("====================================================");
                    Console.ReadLine();
                }
        }
         
        

        public void SearchTransactionHistory()
        {
            Console.WriteLine("Nhập Mã số tài khoản cần khóa: ");
            var AccountNumber = Console.ReadLine();
            var accountExisting = _acountModel.FindByAccountNumber(AccountNumber);
            if (accountExisting.AccountNumber.Length != 0)
            {
                var listHistory = _acountModel.FindTransactionHistoryByIdNumber(accountExisting.AccountNumber);
                foreach (var Transactionhistory in listHistory)
                {
                    Console.WriteLine("Loại giao dịch: "+Transactionhistory.GetType());
                    Console.WriteLine("Số Tiền: "+Transactionhistory.Amount);
                    Console.WriteLine("Thời gian bắt đầu giao dịch: "+Transactionhistory.CreateAt);
                    Console.WriteLine("Người gửi: "+Transactionhistory.SenderAccountNumber);
                    Console.WriteLine("Người Nhận: "+Transactionhistory.ReceiverAccountNumber);
                    Console.WriteLine("Nội dung: "+Transactionhistory.Message);
                    Console.WriteLine("=========================================");
                }
            }
            else
            {
                Console.WriteLine("Tài khoản này không tồn tại");
            }
        }
    }
}