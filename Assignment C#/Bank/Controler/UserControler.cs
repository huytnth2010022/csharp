﻿using System;
using System.Data;
using System.Globalization;
using System.Net.Configuration;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using Bank.Entity;
using Bank.Model;
using Bank.Util;
using Bank.View;

namespace Bank.Controler
{
    public class UserControler : IUserControler
    {
        public static string _AcountNumber;
        AcountModel _model = new AcountModel();

       
        public bool Register()
        {
            bool flag = false;
            Acount account;
            do
            {
                //Validate dữ liệu
                account = GetAccountInformation();
                var errors = account.IsValid();
                flag = account.IsValid().Count == 0;
                if (!flag)
                {
                    foreach (var error in errors)
                    {
                        Console.WriteLine(error.Value);
                    }

                    Console.WriteLine("vui lòng bạn nhập lại thông tin!");
                    Console.ReadLine();
                }
            } while (!flag);

            account.EncryptPassWord();
            Console.WriteLine(account.UserName);
            var result = _model.Save(account);
            if (result)
            {
                Console.WriteLine("Đăng ký thành công");
            }
            else
            {
                Console.WriteLine("Đăng ký thất bại");
            }

            return true;
        }

        public Acount GetAccountInformation()
        {
            Acount acount = new Acount();

            Console.WriteLine("Nhập tài khoản");
            acount.UserName = Console.ReadLine();
            Console.WriteLine("Nhập mật khẩu");
            acount.Password = Console.ReadLine();
            Console.WriteLine("Nhập lại mật khẩu");
            acount.PasswardConfig = Console.ReadLine();

            return acount;
        }

        public void ShowBankInformation()
        {
            throw new System.NotImplementedException();
        }

        public Acount Login()
        {
            Acount acount = new Acount();
            //Lấy thông tin người dùng
            Console.WriteLine("Tài Khoản: ");
            acount.UserName = Console.ReadLine();
            Console.WriteLine("Mật khẩu: ");
            acount.Password = Console.ReadLine();
            //check tài khoản có tồn tại trên DB không đồng thời kéo dữ liệu từ DB để lấy Salt
            var acountDb = _model.FindByAccounUserName(acount.UserName);
            var Account = _model.FindByAccountNumber(acountDb.AccountNumber);
            if (acountDb.UserName.Equals(acount.UserName))
            {
                string newPW = HashUtil.GenerateSaltedSHA1(acount.Password, acountDb.Salt);
           

                if (newPW.Equals(acountDb.Password) && Account.Status ==1)
                {
                    Console.WriteLine("Đăng nhập thành công");
                    _AcountNumber = acountDb.AccountNumber;
                    Acount ac = _model.FindByAccountNumber(acountDb.AccountNumber);

                    if (ac.IdentityNumber.Length == 0)
                    {
                        AddInformation();
                    }
                    else
                    {
                         UserView.GetMainMenu();
                    }
                }
                else
                {
                    Console.WriteLine("Đăng nhập thất bại");
                }
            }
            else
            {
                Console.WriteLine("không tồn tại");
            }

            return acount;
        }

        public void ChangePassword()
        {
            Console.WriteLine("Đăng nhập lại:");
          string username = Console.ReadLine();
            Acount acount = _model.FindByAccounUserName(username);
            Console.WriteLine("Nhập mật khẩu cũ của bạn:");
            string passwordOld = Console.ReadLine();
                Console.WriteLine(passwordOld);
          var  PasswordEqua = HashUtil.GenerateSaltedSHA1(passwordOld, acount.Salt);
          Console.WriteLine(PasswordEqua);
            if (PasswordEqua.Equals(acount.Password))
            {
                Console.WriteLine("Nhập mật khẩu mới: ");
                string passwordNew = Console.ReadLine();
              var  Salt = HashUtil.RandomString(5);
                //Băm Pass với Mì chính
               acount.Password = HashUtil.GenerateSaltedSHA1(passwordNew, Salt);
               _model.UpdatePassword(acount);
            }
            else
            {
                Console.WriteLine("Mật khẩu không hợp lệ");
            }
        }

        public void withDraw()
        {
            Console.WriteLine("Nhập số tiền bạn cần rút: ");
            Double Money = Convert.ToDouble(Console.ReadLine());
            Double amountexisting = _model.FindByAccountNumber(_AcountNumber).Balance;
            if ((amountexisting - Money) < 0)
            {
                Console.WriteLine("Tài khoản của bạn hiện không đủ tiền");
            }
            else
            {
                TransactionHistory transactionHistory = _model.Withdraw(_AcountNumber, amountexisting - Money);
                transactionHistory.CreateAt = DateTime.Now;
                transactionHistory.Amount = Money;
                transactionHistory.SenderAccountNumber = _model.FindByAccountNumber(_AcountNumber).AccountNumber;
                transactionHistory.ReceiverAccountNumber = _model.FindByAccountNumber(_AcountNumber).AccountNumber;
                _model.InsertToDBTransactionHistory(transactionHistory);
                Console.WriteLine("Rút Thành công 😄");
            }
        }

        public void deposit()
        {
            Console.WriteLine("Nhập số tiền bạn cần gửi: ");
            Double amount = Convert.ToDouble(Console.ReadLine());
            Double amountexisting = _model.FindByAccountNumber(_AcountNumber).Balance;
            TransactionHistory transactionHistory = _model.Deposit(_AcountNumber, amountexisting + amount);
            transactionHistory.CreateAt = DateTime.Now;
            transactionHistory.Amount = amount;
            transactionHistory.SenderAccountNumber = _model.FindByAccountNumber(_AcountNumber).AccountNumber;
            transactionHistory.ReceiverAccountNumber = _model.FindByAccountNumber(_AcountNumber).AccountNumber;
            _model.InsertToDBTransactionHistory(transactionHistory);
            Console.WriteLine("Gửi Thành công 😄");
        }

        public void Transfer()
        {
            Console.WriteLine("Nhập số tài khoản người nhận:");
            string Receiver = Console.ReadLine();
            Acount accexisting =_model.FindByAccountNumber(Receiver);
Console.WriteLine( accexisting.LockTransaction);
            if (accexisting.AccountNumber.Length !=0 && accexisting.LockTransaction != 0)
            {
                
                Console.WriteLine("Nhập số tiền bạn cần chuyển: ");
                Double amount = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Nội dung chuyển");
                string message = Console.ReadLine();
                Double Moneyexisting = _model.FindByAccountNumber(_AcountNumber).Balance;
                if (Moneyexisting < amount)
                {
                    Console.WriteLine("Không đủ tiền để chuyển");
                }
                else
                {
                    Console.WriteLine("Thông tin Chuyển tiền của bạn là");
                    Console.WriteLine("Người Nhận: "+ accexisting.firstName);
                    Console.WriteLine("Mã tài khoản nhận: "+accexisting.AccountNumber);
                    Console.WriteLine("Số tiền gửi là: "+ amount);
                    Console.WriteLine("Bạn có muốn chuyển tiền không (y/n)");
                    string choice=Console.ReadLine();
                    if (choice.ToUpper().Equals("Y"))
                    {
                        TransactionHistory transactionHistory = _model.Transfer(_AcountNumber, Moneyexisting - amount);
                        _model.Transfer(accexisting.AccountNumber, accexisting.Balance + amount);
                        transactionHistory.CreateAt = DateTime.Now;
                        transactionHistory.Amount = amount;
                        transactionHistory.SenderAccountNumber = _model.FindByAccountNumber(_AcountNumber).AccountNumber;
                        transactionHistory.ReceiverAccountNumber = _model.FindByAccountNumber(accexisting.AccountNumber).AccountNumber;
                        transactionHistory.Message = message;
                        _model.InsertToDBTransactionHistory(transactionHistory);
                        Console.WriteLine("Chuyển Thành công 😄");
                    }
                    else
                    {
                        Console.WriteLine("Hủy bỏ chuyển tiền thành công!");
                    }
                }
            }
            else
            {
                Console.WriteLine("Không tồn tại tài khoản này hoặc tài khoản này đã khóa");
            }
        }

        public void CheckInformatoin()
        {
            var acount = _model.FindByAccountNumber(_AcountNumber);
            Console.WriteLine("-----Thông tin tài khoản của bạn là-----");
            Console.WriteLine("Tên chủ tài khoản: "+acount.firstName+" "+acount.lastName);
            Console.WriteLine("Số điện thoại: "+acount.Phone);
            Console.WriteLine("Địa chỉ: "+acount.Address);
            Console.WriteLine("Email: "+ acount.Email);
            Console.WriteLine("Ngày sinh: " + acount.Dab);
            Console.WriteLine("Số dư: "+acount.Balance);
            Console.WriteLine("CCCD/CMND: " + acount.IdentityNumber);
            Console.WriteLine("Trạng thái tài khoản: " + acount.GetStatus());
            Console.WriteLine("====================================================");
            Console.ReadLine();

        }

        public void AddInformation()
        {
            Acount acount = new Acount();
            acount.AccountNumber = _AcountNumber;
            Console.WriteLine("Mời bạn nhập các thông tin cá nhân");
            Console.WriteLine("Nhập Họ: ");
            acount.firstName = Console.ReadLine();
            Console.WriteLine("Nhập Tên người dùng: ");
            acount.lastName = Console.ReadLine();
            Console.WriteLine("nhập email: ");
            acount.Email = Console.ReadLine();
            Console.WriteLine("Nhập số điện thoại: ");
            acount.Phone = Console.ReadLine();
            Console.WriteLine("Nhập CCCD/CMND: ");
            acount.IdentityNumber = Console.ReadLine();
            Console.WriteLine("Nhập Địa chỉ: ");
            acount.Address = Console.ReadLine();
            Console.WriteLine("Nhập ngày/tháng/năm sinh:");
            acount.Dab = DateTime.Parse(Console.ReadLine());
            _model.SaveUserInformation(acount);
        }

        public void UpdateInformation()
        {
          CheckInformatoin();
          Console.WriteLine("Bạn có muốn thay đổi Thông tin (y/n)");
          string choice = Console.ReadLine();

          if (choice.ToUpper().Equals("Y"))
          {
              Acount acount = new Acount();
              acount.AccountNumber = _AcountNumber;
              Console.WriteLine("Mời bạn nhập các thông tin cá nhân");
              Console.WriteLine("Nhập Họ: ");
              acount.firstName = Console.ReadLine();
              Console.WriteLine("Nhập Tên người dùng: ");
              acount.lastName = Console.ReadLine();
              Console.WriteLine("nhập email: ");
              acount.Email = Console.ReadLine();
              Console.WriteLine("Nhập số điện thoại: ");
              acount.Phone = Console.ReadLine();
              Console.WriteLine("Nhập CCCD/CMND: ");
              acount.IdentityNumber = Console.ReadLine();
              Console.WriteLine("Nhập Địa chỉ: ");
              acount.Address = Console.ReadLine();
              Console.WriteLine("Nhập ngày/tháng/năm sinh:");
              acount.Dab = DateTime.Parse(Console.ReadLine());
              _model.Update(acount);
              Console.WriteLine("Thay đổi thành công");
          }
          else
          {
              UserView.GetMainMenu();
          }
          
        }

        public void lockTransaction()
        {
            var acc = _model.FindByAccountNumber(_AcountNumber);
            acc.LockTransaction = 0;
            if (_model.LockTransactionActive(acc))
            {
                Console.WriteLine("Khóa chuyển khoản thành công");
            }
        }

        public void UnlockTransaction()
        {
              var acc = _model.FindByAccountNumber(_AcountNumber);
            acc.LockTransaction = 1;
            if (_model.LockTransactionActive(acc))
            {
                Console.WriteLine("Mở khóa chuyển khoản thành công");
            }
        }



        public void CheckTransactionHistory()
        {
            var listHistory = _model.FindTransactionHistoryByIdNumber(_AcountNumber);
            Console.WriteLine("=========================================");
        foreach (var Transactionhistory in listHistory)
           {
               Console.WriteLine("Loại giao dịch: "+Transactionhistory.GetType());
               Console.WriteLine("Số Tiền: "+Transactionhistory.Amount);
               Console.WriteLine("Thời gian bắt đầu giao dịch: "+Transactionhistory.CreateAt);
               Console.WriteLine("Người gửi: "+Transactionhistory.SenderAccountNumber);
               Console.WriteLine("Người Nhận: "+Transactionhistory.ReceiverAccountNumber);
               Console.WriteLine("Nội dung: "+Transactionhistory.Message);
               Console.WriteLine("=========================================");
           }
      
        }
    }
}