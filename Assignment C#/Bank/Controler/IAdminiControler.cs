﻿using Bank.Entity;

namespace Bank.Controler
{
    public interface IAdminiControler
    {
        Admini Login();
        void CreateAdmini();
 
        void ShowListUser();

        void LockUser();
        void UnlockUser();
        void FindUserByAccountNumber();
        void SearchUserByPhone();
        void SearchTransactionHistory();


    }
}