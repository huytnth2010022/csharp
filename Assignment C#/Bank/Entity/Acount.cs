﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Bank.Util;

namespace Bank.Entity
{
    public class Acount
    {
        public string AccountNumber{get; set;}
        public int Title{ get; set; } //cá nhân hay doanh nghiệp
        public double Balance { get; set; }

        public string UserName{ get; set; }
        public string Password{ get; set; }
        public string PasswordHash{ get; set; }
        public string Salt{ get; set; }
        public int LockTransaction{ get; set; }
        public string firstName{ get; set; }
        public string lastName{ get; set; }
        public DateTime Dab{ get; set; }
        public string Email{ get; set; }
        public string IdentityNumber{ get; set; }//CCCD
        public string Phone{ get; set; }
        public string Address{ get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime UpDateAT{ get; set; } 
        public DateTime DeleteAt{ get; set; }
        public int Status{ get; set; }//1 active 2 khóa 3 xóa
        public string PasswardConfig { get; set; }


        public Acount()
        {
            AccountNumber = Guid.NewGuid().ToString();
            CreateAt = DateTime.Now;
            LockTransaction = 1;
            Status = 1;
            Balance = 0;
        }
        public string GetStatus()
        {
            switch (this.Status)
            {
                case 1:
                    return "active";
                case 2:
                    return "lock";
                case 3:
                    return "delete";
                default:
                    return "Null";
            }
            return null;
        }
        public static int GetStatusToInt(string status)
        {
            switch (status)
            {
                case "active":
                    return 1;
                case "lock":
                    return 2;
                case "delete":
                    return 3;
                default:
                    return 0;
            }
            return 0;
        }
        public Dictionary<string, string> IsValid()
        {
            var error = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(this.UserName))
            {
                error.Add("Username","Vui lòng điền tài khoản của bạn!");
            }
            else
            {
                if (string.IsNullOrEmpty(this.Password))
                {
                    error.Add("Password","Vui lòng điền mật khẩu của bạn!");
                }else if ( !Regex.IsMatch(UserName, @"^[a-zA-Z0-9\\S]+$"))
                {
                    error.Add("KeyUser","Vui lòng nhập đúng định dạng(Không có ký tự đặc biệt)!");
                }else if (!Regex.IsMatch(Password, @"^[A-Z][a-zA-Z0-9\\S]+$"))
                {
                    error.Add("KeyPassword","Vui lòng nhập đúng định dạng(Chữ cái đầu phải viết hoa, " +
                                        "không có ký tự đặc biệt)!");
                }
                else if (!Password.Equals(PasswardConfig))
                {
                    error.Add("PasswordConfig","Mật khẩu không khớp!");
                }
            }
            return error;
        }

        public void EncryptPassWord()
        {
            //Tạo Mì chính
            Salt = HashUtil.RandomString(5);
            //Băm Pass với Mì chính
            PasswordHash = HashUtil.GenerateSaltedSHA1(Password, Salt);
        }
    }
}