﻿using System;

namespace Bank.Entity
{
    public class TransactionHistory
    {
 
        public int Type { get; set; } //withdraw(1),desposit(2),transfer(3)
        public double Amount { get; set; }
        public string SenderAccountNumber { get; set; }
        public string  ReceiverAccountNumber { get; set; }
        public string Message { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime UpDateAT { get; set; }
        public DateTime DeleteAt { get; set; }

        public string GetType()
        {
            switch (this.Type)
            {
                case 1:
                    return "withdraw";
                case 2:
                    return "desposit";
                case 3:
                    return "transfer";
                default:
                    return "Null";
            }
            return null;
        }
        public static int GetTypeToInt(string type)
        {
            switch (type)
            {
                case "withdraw":
                    return 1;
                case "desposit":
                    return 2;
                case "transfer":
                    return 3;
                default:
                    return 0;
            }
            return 0;
        }

        public TransactionHistory()
        {
        }
    }
}