﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Bank.Util;

namespace Bank.Entity
{
    public class Admini
    {
        public string Id { get; set; }
        public string PassWord { get; set; }
        public string UserName { get; set; }
        public string Salt { get; set; }
        public DateTime CreateAT { get; set; }
        public DateTime UpDateAT{ get; set; } 
        public DateTime DeleteAt{ get; set; }
        public string PasswordHash { get; set; }
        public string PasswardConfig { get; set; }
        public int Status{ get; set; }//1 active 2 khóa 3 xóa
        public Admini()
        {
            Id = Guid.NewGuid().ToString();
           this.CreateAT = DateTime.Now;
            Status = 1;
        }
        public Dictionary<string, string> IsValid()
        {
            var error = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(this.UserName))
            {
                error.Add("Username","Vui lòng điền tài khoản của bạn!");
            }
            else
            {
                if (string.IsNullOrEmpty(this.PassWord))
                {
                    error.Add("Password","Vui lòng điền mật khẩu của bạn!");
                }else if ( !Regex.IsMatch(UserName, @"^[a-zA-Z0-9\\S]+$"))
                {
                    error.Add("KeyUser","Vui lòng nhập đúng định dạng(Không có ký tự đặc biệt)!");
                }else if (!Regex.IsMatch(PassWord, @"^[A-Z][a-zA-Z0-9\\S]+$"))
                {
                    error.Add("KeyPassword","Vui lòng nhập đúng định dạng(Chữ cái đầu phải viết hoa, " +
                                            "không có ký tự đặc biệt)!");
                }
                else if (!PassWord.Equals(PasswardConfig))
                {
                    error.Add("PasswordConfig","Mật khẩu không khớp!");
                }
            }
            return error;
        }
        public void EncryptPassWord()
        {
            //Tạo Mì chính
            Salt = HashUtil.RandomString(5);
            //Băm Pass với Mì chính
            PasswordHash = HashUtil.GenerateSaltedSHA1(PassWord, Salt);
        }
        
    }
}