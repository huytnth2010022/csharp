﻿using System;
using System.Collections.Generic;
using System.IO;
using Bank.Entity;
using Bank.Util;
using MySqlConnector;

namespace Bank.Model
{
    public class AcountModel : AcountModelInterface
    {
        private string _insert =
            "INSERT INTO account(username,password,acountnumber,salt) VALUES  ('{0}','{1}','{2}','{3}')";

        private string _SelectTransactionHistory =
            "SELECT * FROM transactionhistory Where acountnumber='{0}'";

        private string _delete =
            "DELETE FROM userinformation WHERE acountnumber ='{0}'";

        private string _insertUserinformation =
            "INSERT INTO userinformation(acountnumber,title,balance,lockTransaction,firstName,lastname,phone,email,address,IdentityNumber,dab,createAt,deleteAt,updateAt,Status) VALUES('{0}',{1},{2},{3},'{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}') ";

        private string _selectUserName =
            "SELECT * FROM account Where username = '{0}'";
        private string _selectAdminName =
            "SELECT * FROM accountadmin Where AdminName = '{0}'";

        private string _DWT =
            "UPDATE userinformation SET balance ={0} WHERE acountnumber ='{1}'";

        private string _selectAccountNumber =
            "SELECT * FROM userinformation WHERE acountnumber = '{0}'";

        private string _InsertToDBTransactionHistory =
            "INSERT INTO transactionhistory VALUES ('{0}',{1},'{2}','{3}','{4}','{5}','{6}')";

        private string _LockTransacTionActive =
            "UPDATE userinformation SET lockTransaction ={0} WHERE acountnumber ='{1}'";

        private string _ChangePassword = 
            "UPDATE account SET password ='{0}' WHERE acountnumber ='{1}'";
        private string _SaveAdmin =
            "INSERT INTO accountadmin VALUES ('{0}','{1}','{2}','{3}',{4})";
        private  string _FindAll = 
            "SELECT*FROM userinformation";
        private  string _FindByPhone = 
            "SELECT*FROM userinformation WHERE phone='{0}'";

        private string _LULAccount =
            "UPDATE userinformation SET Status ='{0}' WHERE acountnumber='{1}'";
        public bool Save(Acount acount)
        {
            try
            {
                using (var cnn = ConnectionHelper.GetInstance())
                {
                    cnn.Open();
                    var cmd = new MySqlCommand();
                    cmd.Connection = cnn;
                    cmd.CommandText = string.Format(_insert, acount.UserName, acount.PasswordHash, acount.AccountNumber,
                        acount.Salt);
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = string.Format("INSERT INTO userinformation(acountnumber) VALUE ('{0}')",
                        acount.AccountNumber);
                    cmd.ExecuteNonQuery();
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public bool SaveAdmin(Admini admin)
        {
            try
            {
                using (var cnn = ConnectionHelper.GetInstance())
                {
                    cnn.Open();
                    var cmd = new MySqlCommand();
                    cmd.Connection = cnn;
                    cmd.CommandText = string.Format(_SaveAdmin, admin.Id, admin.UserName,admin.PasswordHash,admin.Salt,admin.Status);
                    cmd.ExecuteNonQuery();
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public bool LockTransactionActive(Acount acc)
        {
            using (var cnn = ConnectionHelper.GetInstance())
            {
                cnn.Open();
                var cmd = new MySqlCommand();
                cmd.Connection = cnn;
                cmd.CommandText = string.Format(_LockTransacTionActive, acc.LockTransaction, acc.AccountNumber);
                cmd.ExecuteNonQuery();
                return true;
            }

            return false;
        }

        public bool SaveUserInformation(Acount ac)
        {
            try
            {
                using (var cnn = ConnectionHelper.GetInstance())
                {
                    cnn.Open();
                    var cmd = new MySqlCommand();
                    cmd.Connection = cnn;
                    cmd.CommandText = string.Format(_delete, ac.AccountNumber);
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = string.Format(_insertUserinformation,
                        ac.AccountNumber,
                        ac.Title,
                        ac.Balance,
                        ac.LockTransaction,
                        ac.firstName,
                        ac.lastName,
                        ac.Phone,
                        ac.Email,
                        ac.Address,
                        ac.IdentityNumber,
                        ac.Dab.ToString(),
                        ac.CreateAt.ToString(),
                        ac.UpDateAT.ToString(),
                        ac.DeleteAt.ToString(),
                        ac.GetStatus());

                    cmd.ExecuteNonQuery();
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        private string _update =
            "UPDATE userinformation SET firstName='{0}',lastname='{1}',email='{2}',phone='{3}',IdentityNumber='{4}',address='{5}',dab='{6}'";

        public bool Update(Acount acount)
        {
            using (var cnn = ConnectionHelper.GetInstance())
            {
                cnn.Open();
                var cmd = new MySqlCommand();
                cmd.Connection = cnn;
                cmd.CommandText = string.Format(_update,
                    acount.firstName,
                    acount.lastName,
                    acount.Email,
                    acount.Phone,
                    acount.IdentityNumber,
                    acount.Address,
                    acount.Dab.ToString());
                cmd.ExecuteNonQuery();
                return true;
            }

            return false;
        }

        public bool UpdatePassword(Acount acc)
        {
            using (var cnn = ConnectionHelper.GetInstance())
            {
                cnn.Open();
                var cmd = new MySqlCommand();
                cmd.Connection = cnn;
                cmd.CommandText = string.Format(_ChangePassword,
            acc.Password,
            acc.AccountNumber);
                cmd.ExecuteNonQuery();
                return true;
            }

            return false;
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Admini FindByAccounAdminName(string name)
        {
            Admini admin = new Admini();
            using (var cnn = ConnectionHelper.GetInstance())
            {
                cnn.Open();
                var cmd = new MySqlCommand();
                cmd.Connection = cnn;
                cmd.CommandText = string.Format(_selectAdminName, name);
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string AdminName = reader.GetString("AdminName");
                            string salt = reader.GetString("Salt");
                            string passw = reader.GetString("Password");
                            string id = reader.GetString("ID");
                            admin.UserName = AdminName;
                            admin.PassWord = passw;
                            admin.Salt = salt;
                            admin.Id = id;

                        }
                    }
                }
            }

            return admin;
        }
        public Acount FindByAccounUserName(string name)
        {
            Acount acount = new Acount();
            using (var cnn = ConnectionHelper.GetInstance())
            {
                cnn.Open();
                var cmd = new MySqlCommand();
                cmd.Connection = cnn;
                cmd.CommandText = string.Format(_selectUserName, name);
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string username = reader.GetString("username");
                            string salt = reader.GetString("salt");
                            string passw = reader.GetString("password");
                            string accountNumber = reader.GetString("acountnumber");
                            acount.AccountNumber = accountNumber;
                            acount.UserName = username;
                            acount.Password = passw;
                            acount.Salt = salt;
                        }
                    }
                }
            }

            return acount;
        }


        public Acount FindByAccountNumber(string accountnumber)
        {
            Acount acount = new Acount();
            using (var cnn = ConnectionHelper.GetInstance())
            {
                cnn.Open();
                var cmd = new MySqlCommand();
                cmd.Connection = cnn;
                cmd.CommandText = string.Format(_selectAccountNumber, accountnumber);
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            int Type = reader.GetInt32("title");
                            double Balance = reader.GetDouble("balance");
                            int lockTransaction = reader.GetInt32("lockTransaction");
                            string firstName = reader.GetString("firstName");
                            string lastname = reader.GetString("lastname");
                            string phone = reader.GetString("phone");
                            string email = reader.GetString("email");
                            string address = reader.GetString("address");
                            string IdentityNumber = reader.GetString("IdentityNumber");
                            string Status = reader.GetString("Status");
                            string accountNumber = reader.GetString("acountnumber");
                            
                            acount.AccountNumber = accountNumber;
                            acount.Title = Type;
                            acount.Balance = Balance;
                            acount.LockTransaction = lockTransaction;
                            acount.firstName = firstName;
                            acount.lastName = lastname;
                            acount.Phone = phone;
                            acount.Email = email;
                            acount.Address = address;
                            acount.IdentityNumber = IdentityNumber;
                            acount.Status = Acount.GetStatusToInt(Status);
                        }
                    }
                }
            }

            return acount;
        }

        public List<Acount> FindAll()
        {
            List<Acount> acounts = new List<Acount>();
            using (var cnn = ConnectionHelper.GetInstance())
            {
                cnn.Open();
                var cmd = new MySqlCommand();
                cmd.Connection = cnn;
                cmd.CommandText = string.Format(_FindAll);
                var reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Acount acount = new Acount();
                        int Type = reader.GetInt32("title");
                        double Balance = reader.GetDouble("balance");
                        int lockTransaction = reader.GetInt32("lockTransaction");
                        string firstName = reader.GetString("firstName");
                        string lastname = reader.GetString("lastname");
                        string phone = reader.GetString("phone");
                        string email = reader.GetString("email");
                        string address = reader.GetString("address");
                        string IdentityNumber = reader.GetString("IdentityNumber");
                        string Status = reader.GetString("Status");
                        string accountNumber = reader.GetString("acountnumber");
                            
                        acount.AccountNumber = accountNumber;
                        acount.Title = Type;
                        acount.Balance = Balance;
                        acount.LockTransaction = lockTransaction;
                        acount.firstName = firstName;
                        acount.lastName = lastname;
                        acount.Phone = phone;
                        acount.Email = email;
                        acount.Address = address;
                        acount.IdentityNumber = IdentityNumber;
                        acount.Status = Acount.GetStatusToInt(Status);
                        acounts.Add(acount);
                    }
                }
            }

            return acounts;
        }

        public bool LockAndUnLockAccount(string status,string name)
        {
            using (var cnn = ConnectionHelper.GetInstance())
            {
                cnn.Open();
                var cmd = new MySqlCommand();
                cmd.Connection = cnn;
                cmd.CommandText = string.Format(_LULAccount, status, name);
                cmd.ExecuteNonQuery();
                return true;
            }
        }
        

        public List<Acount> SearchByPhone(string keyword)
        {
            List<Acount> acounts = new List<Acount>();
            using (var cnn = ConnectionHelper.GetInstance())
            {
                cnn.Open();
                var cmd = new MySqlCommand();
                cmd.Connection = cnn;
                cmd.CommandText = string.Format(_FindByPhone,keyword);
                var reader =cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Acount acount = new Acount();
                        int Type = reader.GetInt32("title");
                        double Balance = reader.GetDouble("balance");
                        int lockTransaction = reader.GetInt32("lockTransaction");
                        string firstName = reader.GetString("firstName");
                        string lastname = reader.GetString("lastname");
                        string phone = reader.GetString("phone");
                        string email = reader.GetString("email");
                        string address = reader.GetString("address");
                        string IdentityNumber = reader.GetString("IdentityNumber");
                        string Status = reader.GetString("Status");
                        string accountNumber = reader.GetString("acountnumber");

                        acount.AccountNumber = accountNumber;
                        acount.Title = Type;
                        acount.Balance = Balance;
                        acount.LockTransaction = lockTransaction;
                        acount.firstName = firstName;
                        acount.lastName = lastname;
                        acount.Phone = phone;
                        acount.Email = email;
                        acount.Address = address;
                        acount.IdentityNumber = IdentityNumber;
                        acount.Status = Acount.GetStatusToInt(Status);
                        acounts.Add(acount);
                    }
                }
            }

            return acounts;
        }

        public List<Acount> SearchByIdentityNumber(string keyword, int page, int limit)
        {
            throw new NotImplementedException();
        }

        public List<TransactionHistory> FindTransactionHistoryByIdNumber(string accountNumber)
        {
            List<TransactionHistory> transactionHistories = new List<TransactionHistory>();
           
            using (var cnn = ConnectionHelper.GetInstance())
            {
                cnn.Open();
                var cmd = new MySqlCommand();
                cmd.Connection = cnn;
                cmd.CommandText = string.Format(_SelectTransactionHistory, accountNumber);
                var reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        TransactionHistory transactionHistory = new TransactionHistory();
                        string SenderAccountNumber = reader.GetString("SenderAccountNumber");
                        string ReceiverAccountNumber = reader.GetString("ReceiverAccountNumber");
                        Double Balance = reader.GetDouble("Balance");
                        string Type = reader.GetString("Type");
                        string Message = reader.GetString("Message");
                        string startTime = reader.GetString("startTime");
                        
                        transactionHistory.SenderAccountNumber = SenderAccountNumber;
                        transactionHistory.ReceiverAccountNumber = ReceiverAccountNumber;
                        transactionHistory.Amount = Balance;
                        transactionHistory.Type = TransactionHistory.GetTypeToInt(Type);
                        transactionHistory.Message = Message;
                        transactionHistory.CreateAt = DateTime.Parse(startTime);
                       
                        transactionHistories.Add(transactionHistory);
                    }
            
                }

               
            }

            return transactionHistories;
        }

        public void InsertToDBTransactionHistory(TransactionHistory T)
        {
            using (var cnn = ConnectionHelper.GetInstance())
            {
                cnn.Open();
                var cmd = new MySqlCommand();
                cmd.Connection = cnn;
                cmd.CommandText = string.Format(_InsertToDBTransactionHistory,
                    T.SenderAccountNumber,
                    T.Amount,
                    T.GetType(),
                    T.CreateAt.ToString(),
                    T.SenderAccountNumber,
                    T.ReceiverAccountNumber,
                    T.Message);
                cmd.ExecuteNonQuery();
            }
        }


        public TransactionHistory Deposit(string accountNumber, double amount)
        {
            using (var cnn = ConnectionHelper.GetInstance())
            {
                TransactionHistory transactionHistory = new TransactionHistory();
                cnn.Open();
                var cmd = new MySqlCommand();
                cmd.Connection = cnn;
                cmd.CommandText = string.Format(_DWT, amount, accountNumber);

                cmd.ExecuteNonQuery();
                transactionHistory.Type = 2;
                return transactionHistory;
            }
        }


        public TransactionHistory Withdraw(string accountNumber, double amount)
        {
            using (var cnn = ConnectionHelper.GetInstance())
            {
                TransactionHistory transactionHistory = new TransactionHistory();
                cnn.Open();
                var cmd = new MySqlCommand();
                cmd.Connection = cnn;
                cmd.CommandText = string.Format(_DWT, amount, accountNumber);
                cmd.ExecuteNonQuery();
                transactionHistory.Type = 1;
                return transactionHistory;
            }
        }

        public TransactionHistory Transfer(string sendAccountNumber, double amount)
        {
            using (var cnn = ConnectionHelper.GetInstance())
            {
                TransactionHistory transactionHistory = new TransactionHistory();
                cnn.Open();
                var cmd = new MySqlCommand();
                cmd.Connection = cnn;
                cmd.CommandText = string.Format(_DWT, amount, sendAccountNumber);
                cmd.ExecuteNonQuery();
                transactionHistory.Type = 3;
                return transactionHistory;
            }
        }
    }
}