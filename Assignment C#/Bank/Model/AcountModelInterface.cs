﻿using System;
using System.Collections.Generic;
using Bank.Entity;

namespace Bank.Model
{
    public interface AcountModelInterface
    {
        bool Save(Acount acount);
        bool SaveAdmin(Admini admini);
        bool LockTransactionActive(Acount acc);
        bool SaveUserInformation(Acount ac);
         bool Update(Acount acount);//thay đổi tài khoản
         bool UpdatePassword(Acount acc);
        bool Delete(int id);
        Admini FindByAccounAdminName(string name);
       Acount FindByAccounUserName(string name);
        Acount FindByAccountNumber(string id);//tìm theo số tài khoản
        List<Acount> FindAll();//lấy danh sách kèm phân trang
        bool LockAndUnLockAccount(string status,string name);

        List<Acount> SearchByPhone(string keyword);//lọc theo số điện thoại(1 phần số điện thoại)
        List<Acount> SearchByIdentityNumber(string keyword, int page, int limit);//lọc theo cccd
      //sao kê
        List<TransactionHistory>  FindTransactionHistoryByIdNumber(string accountNumber);
//Thực hiện Ghi lại thông tin Gửi,Chuyển,Rút
        void InsertToDBTransactionHistory(TransactionHistory T);
        //thực hiện gửi tiền
      TransactionHistory Deposit(string accountNumber, double amount);
      //thực hiện rút tiền
      TransactionHistory Withdraw(string accountNumber, double amount);
      //thực hiện chuyển khoản
      TransactionHistory Transfer(
          string sendAccountNumber,
          double amount
        );

    }
}